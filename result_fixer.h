#ifndef RESULT_FIXER_INCLUDED
#define RESULT_FIXER_INCLUDED

#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/opencv.hpp"
#include <string>
#include <vector>
#include <fstream>
#include "table_printer.h"
#include <map>
#include <queue> 
#include "functions.h"
class result_fixer
{
public:
	void launch(std::vector< cv::Mat>& ) const ;
private:
	void fix_border( cv::Mat&) const;
	void flood_fill( cv::Mat& , int , int) const;
	void remove_empty_results(std::vector< cv::Mat> &) const;
	bool is_empty( const cv::Mat&) const;
	void leave_biggest_segment( cv::Mat&) const ;	
	void drop_multiple_segments( std::vector< cv::Mat>& ) const;
	int number_of_segments( const cv::Mat&) const;
};


#endif