#ifndef APPROACHES_INCLUDED
#define APPROACHES_INCLUDED
#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/opencv.hpp"
#include "contour_fixer.h"
#include <string>
#include <vector>



void parvo_bilateralFilter_dilation_erosion_canny_thicken_svm_approach( const cv::Mat&, std::vector< cv::Mat>* = nullptr);//ok contour
void parvo_bilateralFilter_dilation_erosion_kmeans_canny_thicken_svm_approach( const cv::Mat&, std::vector< cv::Mat>* = nullptr);//no
void parvo_bilaeralFilter_dilation_erosion_kmeans_canny_fix_grabcut_approach( const cv::Mat&, std::vector< cv::Mat>* = nullptr);//not bad, needs work though

void medianCut_adaptiveThresholding_approach( const cv::Mat&, std::vector< cv::Mat>* = nullptr); // good enough
//void parvo_vilateralFilter_adaptiveThresholding_
void RGBtoHSV_medianFilter_dilation_erosion_value_canny_approach( const cv::Mat&, std::vector< cv::Mat>* = nullptr);//colorful and not bad

void approach1( const cv::Mat&, std::vector< cv::Mat>* = nullptr); //again, not bad

void fusion( const cv::Mat&);


#endif


