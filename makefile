SRCS = melanoma.cpp  functions.cpp  approaches.cpp contour_fixer.cpp result_fixer.cpp border_detector.cpp feature_detector.cpp features.cpp

TOP = .

CPLUSPLUS = c++
LD		  = c++
RM		  = rm -f
MV		  = mv
MKDIR     = mkdir -p

INCPATH		= -I
CXXFLAGS_X	= -g -O0 -mfpmath=sse -msse3 -Wall
CXXFLAGS	= -pipe -fPIC -std=c++0x -msse3 `pkg-config opencv --cflags`
LIB_PATH	= 
LIBS	    = `pkg-config opencv --libs`

TARGET_BIN = $(BINDIR)/exec
	
#########################################
.SUFFIXES: .c .cpp .mk .o .h
#########################################
OBJDIR := $(TOP)/obj
BINDIR := $(TOP)/bin
LIBDIR := $(TOP)/lib

src-cpp:=$(filter %.cpp,$(SRCS))
SRCDEPENDS:= $(src-cpp:%.cpp=$(OBJDIR)/%.mk)
CXXLIBOBJS = $(src-cpp:%.cpp=$(OBJDIR)/%.o)

.PRECIOUS: %/.dir
%/.dir:
	@$(MKDIR) $(@D)
	@touch $@

$(OBJDIR)/%.mk : %.cpp $(OBJDIR)/.dir
	$(CPLUSPLUS) $(INCPATH) $(CXXFLAGS) -MM -MT'$(OBJDIR)/$*.o' -MT'$(OBJDIR)/$*.lo' $< > $@

#########################################
#  compilation rules
#########################################
$(TARGET_BIN): $(BINDIR)/.dir $(OBJDIR)/.objs
	TMP_EXEC=`mktemp exec_XXXXXX`; \
chmod ugo+r $$TMP_EXEC; \
$(LD) $$(cat $(OBJDIR)/.objs) -o $$TMP_EXEC $(LIB_PATH) $(LIBS); \
$(MV) -f $$TMP_EXEC $@

$(OBJDIR)/%.o : %.cpp $(OBJDIR)/.dir
	cd $(@D) ; $(CPLUSPLUS) $(INCPATH) -I$(CURDIR) -I$(CURDIR)/../.. $(CXXFLAGS_X) $(CXXFLAGS) -o $(@F) -c $(CURDIR)/$<

$(OBJDIR)/.objs: $(CXXLIBOBJS) $(OBJDIR)/.dir
	@echo "$(CXXLIBOBJS)" > $(OBJDIR)/.objs

clean:
	$(RM) -r $(TOP)/obj/ $(TOP)/bin/

-include $(SRCDEPENDS)
