#ifndef FUNCTIONS_INCLUDED
#define FUNCTIONS_INCLUDED

#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/opencv.hpp"
#include <string>
#include <vector>
#include <fstream>
#include "table_printer.h"
#include <map>
#include <queue> 

static cv::Vec3b pure_white = cv::Vec3b( 255,255,255);
static cv::Vec3b pure_black = cv::Vec3b( 0,0,0);
static cv::Vec3b pure_red = cv::Vec3b( 255,0,0);
static cv::Vec3b pure_green = cv::Vec3b(0,255,0);
static cv::Vec3b pure_blue = cv::Vec3b( 0,0,255);
void new_window( const cv::Mat&, const std::string&  = "image");
void retina_parvo( const cv::Mat&, cv::Mat& );
void otsu_threshold( const cv::Mat&, cv::Mat&);
void generate_samples( const cv::Mat&, cv::Mat&);
bool is_valid_coordinate( const cv::Mat&, int , int);
void prepare_mask_for_grabcut( const cv::Mat&, cv::Mat&);

bool is_white( const cv::Mat& , int , int );
bool is_black( const cv::Mat&, int , int );

void get_bin_mask( const cv::Mat&, cv::Mat&);

void prepare_samples_for_svm( const cv::Mat&, const cv::Mat&, cv::Mat&, cv::Mat&);\

void prepare_mask_for_grabcut_pixel_check( const cv::Mat&, const cv::Mat&, cv::Mat&);

int get_white_count_in_all_directions( const cv::Mat&, int , int );
bool white_exists_in_current_direction( const cv::Mat&, int , int , int , int );

void remove_channel_keep_rgb( const cv::Mat&, cv::Mat&, int);

void medianCut( const cv::Mat&, cv::Mat&, std::vector< cv::Vec3b>&);

int color_distance( const cv::Vec3b&, const cv::Vec3b&);

float apply_kernel_on_pixel( const cv::Mat&, int , int, const cv::Mat&, int);

void dilate_erode( const cv::Mat&, cv::Mat&, int, int);

void initiate_grabcut( const cv::Mat&, const cv::Mat&, cv::Mat&);

void prepare_mask_for_grabcut_contour( const cv::Mat&, cv::Mat& );

void get_bin_from_color( const cv::Mat&, cv::Mat&);
void get_color_from_bin( const cv::Mat&, const cv::Mat&, cv::Mat&);

bool is_on_edge( const cv::Mat&, int , int );

void get_final_bin( const std::vector< cv::Mat>& , cv::Mat&, int);

void draw_contour(const cv::Mat&, const cv::Mat&, cv::Mat&);

void remove_bad_samples( std::vector< cv::Mat> &);

int white_area( const cv::Mat&);
#endif