#include "contour_fixer.h"

void fixer::erosion_dilation_fix( const cv::Mat& image, cv::Mat& destination, int erode_count, int dilate_count)
{
	cv::Mat eroded;
	cv::erode( image, eroded, cv::Mat(), cv::Point( -1,-1) , erode_count);
	cv::Mat dilated;
	cv::dilate( eroded, dilated, cv::Mat(), cv::Point( -1, -1), dilate_count);
	destination = dilated.clone();
}