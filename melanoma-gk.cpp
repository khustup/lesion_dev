#include <iostream>

//#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include "bloor.hpp"

using namespace cv;
using namespace std;


Mat res;
Mat img;

int contrast = 128;
int brightness = 128;
int canny_threshold = 1;
int slider_max_tick = 255;

void
edge_detect_Sobel (const Bloor& img, Mat& res)
{
    img.blur(res);
    Mat colors[3];

    cvtColor (res, res, COLOR_BGR2RGB);
    split(res,colors);
    res = colors[0]-(colors[1]+colors[2])/2;
    /// Generate grad_x and grad_y
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;
    int ddepth = CV_16S;
    int scale = 1;
    int delta = 0;

    /// Gradient X
    Sobel (res, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);
    convertScaleAbs (grad_x, abs_grad_x);

    /// Gradient Y
    Sobel (res, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT);
    convertScaleAbs (grad_y, abs_grad_y);

    /// Total Gradient (approximate)
    addWeighted (abs_grad_x, 0.5, abs_grad_y, 0.5, 0, res);
    normalize (res, res, 0, 255, NORM_MINMAX, CV_8U);
}

void
edge_detect_Scharr (const Bloor& img, Mat& res)
{
    img.blur(res);
    cvtColor (res, res, COLOR_BGR2GRAY);
    /// Generate grad_x and grad_y
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;
    int ddepth = CV_16S;
    int scale = 1;
    int delta = 0;

    /// Gradient X
    Scharr (res, grad_x, ddepth, 1, 0, scale, delta, BORDER_DEFAULT);
    convertScaleAbs (grad_x, abs_grad_x);

    /// Gradient Y
    Scharr (res, grad_y, ddepth, 0, 1, scale, delta, BORDER_DEFAULT);
    convertScaleAbs (grad_y, abs_grad_y);

    /// Total Gradient (approximate)
    addWeighted (abs_grad_x, 0.5, abs_grad_y, 0.5, 0, res);
}

void
edge_detect_Canny (const Bloor& img, Mat& res)
{
    img.blur(res);

    int kernel_size = 3;
    cvtColor (res, res, COLOR_BGR2GRAY);

    /// Canny detector
    Canny( res, res, canny_threshold, 3*canny_threshold, kernel_size );
}

void
on_slide (int, void*)
{

    Mat drawing = Mat::zeros (res.size (), res.type ());
    res.convertTo (drawing, -1, (contrast - 128) / 12.7, brightness - 128);

    /*
     for( int y = 0; y < src_gray.rows; y++ )
     for( int x = 0; x < src_gray.cols; x++ )
     for( int c = 0; c < 1; c++ );
     //drawing.at<unsigned char>(y, x) = src_gray.at<unsigned char>(y, x) * (contrast-128)/12.7 + (brightness-128);
     //drawing.at<Vec3b>(y,x)[c] = saturate_cast<uchar>( 1*( src_gray.at<Vec3b>(y,x)[c] ) + 0 );
     */

    namedWindow ("Contours", WINDOW_AUTOSIZE);
    imshow ("Contours", drawing);

}

void
on_slide_canny (int, void* bimg_)
{

    //Mat drawing = Mat::zeros (res.size (), res.type ());
    Bloor bimg = *(Bloor*) bimg_;
    edge_detect_Canny (bimg, res);


    namedWindow ("Contours", WINDOW_AUTOSIZE);
    imshow ("Contours", res);

}


int
main (int argc, const char** argv)
{
    const char* src_window = "MyWindow";

    img = imread (argv[1], IMREAD_UNCHANGED);

    if (img.empty ()) {
	cout << "Error : Image cannot be loaded..!!" << endl;
	return -1;
    }

    BlurPoster bimg{};

    bimg.set_src(img);
    //bimg.blur(res);
    edge_detect_Sobel(bimg, res);

    namedWindow (src_window, WINDOW_AUTOSIZE);
    imshow (src_window, res);

    //createTrackbar ("Contrast:", src_window, &contrast, slider_max_tick, on_slide);
    //createTrackbar ("Brightness:", src_window, &brightness, slider_max_tick, on_slide);
    //createTrackbar ("Canny:", src_window, &canny_threshold, slider_max_tick, on_slide_canny, (void*)&bimg);

    waitKey (0);
    destroyWindow (src_window);
    on_slide (0, 0);
    return 0;
}
