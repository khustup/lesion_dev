#include "approaches.h"
#include "functions.h"

void bilateralFilter_dilation_erosion_canny_approach( const cv::Mat& original_image, std::vector<cv::Mat>* results)
{
	cv::Mat image = original_image.clone();
	cv::Mat filtered;
	cv::bilateralFilter( image, filtered, 15,80,80);
	image = filtered.clone();
	new_window( filtered, "filtered");
	cv::Mat eroded, dilated;

	cv::dilate( image, dilated, cv::Mat(), cv::Point(-1,-1), 1);
	image = dilated.clone();
	cv::dilate( image, dilated,cv::Mat(), cv::Point(-1,-1), 1);
	new_window( dilated, "dialted");

	cv::Mat eroded_gray;
	cvtColor(eroded,  eroded_gray, CV_RGB2GRAY);
	cv::Mat edges =  eroded_gray.clone();
	cv::Canny( edges, edges, 30, 90,3);
	new_window( edges);

	std::vector< std::vector< cv::Point> > contours;
	std::vector< cv::Vec4i> hierarchy;

	findContours( edges, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );
	cv::Mat result = cv::Mat::zeros( edges.size(), CV_8UC3);

	for(int  i = 0; i < contours.size(); ++i){
		cv::drawContours( result, contours, i , cv::Scalar( 255,255,255), 4, 8, hierarchy, 0 , cv::Point());
	}

	new_window( result, "result");
	
}
void parvo_bilateralFilter_dilation_erosion_canny_thicken_svm_approach( const cv::Mat& original_image, std::vector<cv::Mat>* results)
{
	new_window( original_image, "original_image");
	cv::Mat image = original_image.clone();
	cv::Mat parvo;
	//retina_parvo( image, parvo);
	parvo = image.clone();
	cv::Mat filtered;

	cv::bilateralFilter(parvo, filtered, 5, 80, 80 );
	new_window( filtered, "filtered");
	cv::Mat dilated;
	cv::dilate( parvo, dilated, cv::Mat(), cv::Point( -1,-1), 1);
	new_window( dilated, "dilated");
	cv::Mat eroded;
	cv::erode( dilated, eroded, cv::Mat(), cv::Point( -1, -1), 1);
	new_window( eroded, "eroded");
	cv::Mat eroded_gray;
	cvtColor(eroded,  eroded_gray, CV_RGB2GRAY);
	cv::Mat edges =  eroded_gray.clone();
	cv::Canny( edges, edges, 30, 90,3);
	new_window( edges);

	std::vector< std::vector< cv::Point> > contours;
	std::vector< cv::Vec4i> hierarchy;

	findContours( edges, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );
	cv::Mat result = cv::Mat::zeros( edges.size(), CV_8UC3);

	for(int  i = 0; i < contours.size(); ++i){
		cv::drawContours( result, contours, i , cv::Scalar( 255,255,255), 4, 8, hierarchy, 0 , cv::Point());
	}
	new_window( result, "canny result");
	cv::Mat samples, labels;
	prepare_samples_for_svm( dilated, result, samples, labels);
	cv::SVM svm;
	if(svm.train( samples, labels)){
		std::cout<<"trained the classifier";
	} else {
		std::cout<<"didint train the classifier";
	}
	cv::Mat newImage = original_image.clone();
	int totalWhite = 0;
	for(int  i = 0; i < image.rows; ++i){
		for(int j = 0; j < image.cols; ++j){
			cv::Mat curSample(1,3, CV_32F);
			for(int k = 0; k < 3; ++k){
				curSample.at< float> ( 0, k) = static_cast<float> (dilated.at< cv::Vec3b>(i,j).val[k]);
				float res = svm.predict( curSample);
				if( res == 1){
					newImage.at<cv::Vec3b>(i,j) = pure_white;
					++totalWhite;
				} 
			}
		}
	}
	std::cout<<totalWhite;
	new_window( newImage, "newImag");
}


void medianCut_adaptiveThresholding_approach( const cv::Mat& original_image, std::vector<cv::Mat>* results)
{
	cv::Mat image = original_image.clone();
	std::vector< cv::Vec3b> colorSet;
	for(int  i = 0;  i < 8; ++i){
		for(int j = 0; j < 8; ++j){
			for(int k = 0; k < 8; ++k){
				colorSet.push_back( cv::Vec3b( i * 16,j * 16,k * 16));
			}
		}
	}
	cv::Mat medianCutRes;
	medianCut( image, medianCutRes, colorSet);
	new_window( medianCutRes, "median cut result");
	cv::Mat gray;
	cv::cvtColor( medianCutRes, gray, CV_RGB2GRAY);
	cv::Mat result;
	cv::adaptiveThreshold( gray, result, 255,cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 3, 0 );
	new_window( result, "result");
}


void RGBtoHSV_medianFilter_dilation_erosion_value_canny_approach( const cv::Mat& original_image, std::vector<cv::Mat>* results)
{
	cv::Mat image = original_image.clone();
	cv::Mat hsvImage;
	cv::cvtColor( image, hsvImage, CV_BGR2HSV);
	cv::Mat eroded, dilated; 

	cv::dilate( hsvImage, dilated, cv::Mat(), cv::Point(-1,-1), 1);
	cv::erode( dilated, eroded, cv::Mat(), cv::Point(-1,-1), 1);

	new_window( eroded, "dilated");

	cv::Mat eroded_gray(image.size(), CV_8UC1);
	for(int  i = 0;  i < image.rows; ++i){
		for(int j = 0; j < image.cols; ++j){
			eroded_gray.at<uchar>(i,j) = eroded.at<cv::Vec3b>(i,j).val[2];
		}
	}
	cv::Mat edges =  eroded_gray.clone();
	cv::Canny( edges, edges, 50, 100,3);
	new_window( edges);

	std::vector< std::vector< cv::Point> > contours;
	std::vector< cv::Vec4i> hierarchy;

	findContours( edges, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );
	cv::Mat result = cv::Mat::zeros( edges.size(), CV_8UC3);

	for(int  i = 0; i < contours.size(); ++i){
		cv::drawContours( result, contours, i , cv::Scalar( 255,255,255), 2, 8, hierarchy, 0 , cv::Point());
	}
	new_window( result, "result");
}

void fusion( const cv::Mat& original_image)
{
	std::vector< cv::Mat> results;
	cv::Mat final_result;
}