#include <fstream>
#include <vector>
#include <cmath> 
#include "border_detector.h"
#include "functions.h"
#include "features.h"
#include "detector_approaches.h"
#include "feature_detector.h"

int main( int argc, char** argv)
{
	cv::Mat original_image = cv::imread("./data/2.jpg");
	cv::Mat image = cv::imread(argv[1]);
	new_window(image, "initial image");
	if (!image.data) {
		std::cout<<"invad image";
		return -1;
	}
	std::vector<cv::Mat> results;

//	approach* a = new mixedThresholding();
//	a->launch(image, &results);




	std::cout<< "statring the long  border detection process"<< std::endl;

	approach* detector = new cannyFix3();
	detector->launch(image, &results);
	delete detector;
	cv::Mat bin = results[0].clone();
	new_window(bin, "bin");
	std::cout<<"finished the border detectin process, about to extract some features" << std::endl;
	features features(original_image,bin);
	auto res = features.detect();
	for (auto i = res.begin(); i != res.end(); ++i) {
		std::cout<< i->first<< "      "<<i->second << std::endl;
	}


	return 0;
}
/*
	cv::Mat image = cv::imread(argv[1]);
	if( !image.data){
		std::cout<<"invalid image";
		return -1;
	}	
	cv::SVM classifier;
	classifier.load("trainer.xml");

	cv::Mat hsv, hsl, lab;

	cv::cvtColor( image, hsv, CV_BGR2HSV );
	cv::cvtColor( image, hsl, CV_BGR2HLS);
	cv::cvtColor(image, lab, CV_BGR2Lab);


	for(int  i = 0; i < image.rows; ++i){
		for(int  j = 0; j < image.cols; ++j){
			cv::Mat cur_sample(1, 12, CV_32F);
			int cur_index = 0;
			for(int  k = 0; k < 3; ++k){
				cur_sample.at<float>(0, cur_index) = static_cast<float>(image.at<cv::Vec3b>(i,j).val[k]);
				++cur_index;
			}
			for(int  k = 0; k < 3; ++k){
				cur_sample.at<float>(0, cur_index) = static_cast<float>(hsv.at<cv::Vec3b>(i,j).val[k]);
				++cur_index;
 			}
			for(int  k = 0; k < 3; ++k){
				cur_sample.at<float>(0, cur_index) = static_cast<float>(hsl.at<cv::Vec3b>(i,j).val[k]);
				++cur_index;
			}
			for(int  k = 0; k < 3; ++k){
				cur_sample.at<float>(0, cur_index) = static_cast<float>(lab.at<cv::Vec3b>(i,j).val[k]);
				++cur_index;
			}
			double res = classifier.predict( cur_sample);

		//	std::cout<< res;
			if( res == -1){
				image.at< cv::Vec3b >(i,j) = pure_white; 
			} else {
				image.at< cv::Vec3b>(i,j) = pure_black;
			}
		}
	}
	new_window(image, "result image, hopefully a ghood one ");

	return 0;
}


*/	
/*



cv::Mat image = cv::imread(argv[1]);
	if( !image.data){
		std::cout<<"invalid image";
		return -1;
	}	

	cv::Mat training_data( 0,12, CV_32F);
	cv::Mat labels(0,1,CV_32F);
	for(int  z = 1; z <= 8; ++z){

		cv::Mat image = cv::imread("./data/" + std::to_string(z) + ".jpg");
		cv::Mat mask = cv::imread( "./data/" + std::to_string(z) + " (copy).jpg");
		cv::Mat hsv, hsl, lab;
		new_window( image, "originla image");
		new_window( mask, "masl");
		cv::cvtColor( image, hsv, CV_BGR2HSV );
		cv::cvtColor( image, hsl, CV_BGR2HLS);
		cv::cvtColor(image, lab, CV_BGR2Lab);
		for(int i = 0; i < mask.rows; ++i){
			for(int j = 0; j < mask.cols; ++j){
				if( mask.at< cv::Vec3b>(i,j) == pure_white || mask.at<cv::Vec3b>(i,j) == pure_black){
					cv::Mat cur_label(1,1,CV_32F);
					if( mask.at< cv::Vec3b>(i,j) == pure_black){
						cur_label.at<float>(0,0) = 1;
					} else {
						cur_label.at<float>(0,0) = -1;
					}


					labels.push_back(cur_label);

					cv::Mat cur_sample(1, 12, CV_32F);

					int cur_index = 0;
					for(int  k = 0; k < 3; ++k){
						cur_sample.at<float>(0, cur_index) = static_cast<float>(image.at<cv::Vec3b>(i,j).val[k]);
						++cur_index;
					}

					for(int  k = 0; k < 3; ++k){
						cur_sample.at<float>(0, cur_index) = static_cast<float>(hsv.at<cv::Vec3b>(i,j).val[k]);
						++cur_index;
					}

					for(int  k = 0; k < 3; ++k){
						cur_sample.at<float>(0, cur_index) = static_cast<float>(hsl.at<cv::Vec3b>(i,j).val[k]);
						++cur_index;
					}

					for(int  k = 0; k < 3; ++k){
						cur_sample.at<float>(0, cur_index) = static_cast<float>(lab.at<cv::Vec3b>(i,j).val[k]);
						++cur_index;
					}
					training_data.push_back( cur_sample );					
				}
			}
		}
	}
	cv::SVM svm;
	svm.train( training_data, labels, cv::Mat(), cv::Mat());
	svm.save( "trainer.xml");
	return 0;
}

*/


/*

























	cv::Mat temp = cv::imread( argv[1]);
	std::cout<< white_area( temp);
	std::freopen("./data/size.txt","r",stdin);
	int from , to;
	std::cin>>from>>to;
	for(int  i = from; i <= to; ++i){
		std::string imagePath = "./data/" + std::to_string(i) + ".jpg";
		cv::Mat image = cv::imread( imagePath);
		border_detector detector(image);
		detector.launch();
		cv::Mat contoured = detector.get_contoured_image();
	//	new_window( contoured, std::to_string(i));
	//	cv::imwrite("./result/" + std::to_string(i) + ".jpg", image);
//		cv::imwrite("./result/" + std::to_string(i) + "_reuslt.jpg", contoured); 
		cv::imwrite("./bin_results/" + std::to_string(i) + "_bin.jpg", detector.get_bin_image());

	}
	return 0;
} 



*/

