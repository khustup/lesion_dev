#include "feature_detector.h"

feature_detector::feature_detector(const cv::Mat& image, const cv::Mat& contoured_image): m_image( image.clone())
																						, m_contoured_image(contoured_image.clone())
																						, m_bin_image(m_image.size(), CV_8UC1)
																						, m_fgPixels_count( 0)
																						, m_bgPixels_count(0)
{
	m_bin_image.setTo( 255);
	bool ** mark = new bool*[image.rows];
	for(int i = 0 ; i < image.rows; ++i){
		mark[i] = new bool[image.cols];
		for(int  j = 0; j < image.cols; ++j){
			mark[i][j] = false;
		}
	}
	new_window( m_bin_image, "bin image before the mian processign");
	std::queue< cv::Point> q;
	mark[0][0] = true;
	q.push( cv::Point( 0,0));
	int px[8] = {0,0,1,-1,1,-1,1,-1};
	int py[8] = {1,-1,0,0,1,-1,-1,1};
	++m_bgPixels_count;
	while( !q.empty())
	{
		int x = q.front().x;
		int y = q.front().y;
		q.pop();
		for(int i = 0; i < 8; ++i){
			int newX = x + px[i];
			int newY = y + py[i];
			if( is_valid_coordinate( image, newX, newY) && ! mark[newX][newY]){
				mark[newX][newY] = true;
				++m_bgPixels_count;
				if( contoured_image.at< cv::Vec3b>(newX, newY) == pure_green){
					m_contour.push_back( cv::Point( newY, newX));	
				} else {
					m_bin_image.at<uchar> (newX, newY) = 0;
					q.push( cv::Point( newX, newY));
				}
			}
		}
	}
	m_fgPixels_count = image.rows  * image.cols  - m_bgPixels_count;
	for(int  i = 0; i < image.rows; ++i){
		delete[] mark[i];
	}
	delete[] mark;
	for(int  i = 0; i < m_bin_image.rows; ++i){
		for(int  j = 0; j < m_bin_image.cols; ++j){
			if( m_bin_image.at<uchar>(i,j) == 0){
				m_bgPixels.push_back(cv::Point( i,j));
			} else {
				m_fgPixels.push_back(cv::Point( i,j));
			}
		}
	}
}

feature_detector::~feature_detector() {}

//**************    Irregularity detector   ************//////

irregularity_index_detector::irregularity_index_detector( const cv::Mat& image, const cv::Mat& contoured_image): feature_detector( image, contoured_image) {}

irregularity_index_detector::~irregularity_index_detector() {}

double irregularity_index_detector::get_feature() const
{
	new_window( m_contoured_image , "contoured image");
	new_window( m_bin_image, "binary image");
	double contourArea = m_fgPixels_count;
	double contourPerimeter = m_contour.size();
	double index = (contourPerimeter * contourPerimeter )/ ( 4 * PI * contourArea );
	std::cout<< "area  "<< contourArea<<"   perimeter   "<< contourPerimeter<<std::endl;
	return index; 
}
	
//*******************8 red variance detector  **////

color_variance_detector::color_variance_detector(const cv::Mat& image, const cv::Mat& contoured_image, int channel): feature_detector( image, contoured_image)
																												   , m_channel(channel)		
{}

color_variance_detector::~color_variance_detector() {}

double color_variance_detector::get_feature() const
{	
	double res = 0;
	double average_color = 0;
	for(int i = 0; i < m_image.rows; ++i){
		for(int j = 0; j < m_image.cols; ++j){
			if( m_bin_image.at<uchar>(i,j) == 255){
				average_color += m_image.at<cv::Vec3b> (i,j).val[m_channel];
			}
		}
	}
	average_color /= m_fgPixels_count;
	for(int i = 0; i < m_image.rows; ++i){
		for(int j = 0; j < m_image.cols; ++j){
			if( m_bin_image.at<uchar>( i,j) == 255){
				double curColor = static_cast<double>(m_image.at< cv::Vec3b >(i,j).val[m_channel]);
				res += ( curColor - average_color) * ( curColor - average_color);  
			}
		}
	}
	res /= ( m_fgPixels_count * m_fgPixels_count);
	return res;
}

//


color_relative_cromaticity::color_relative_cromaticity(const cv::Mat& image, const cv::Mat& contoured_image, int channel): feature_detector( image, contoured_image)
																														 , m_channel(channel)
{}

color_relative_cromaticity::~color_relative_cromaticity()  {}

double color_relative_cromaticity::get_feature() const
{
	double skinColor[3] = {0,0,0};
	double melColor[3] = {0,0,0};
	
	for(int  i = 0; i < m_fgPixels.size(); ++i){
		cv::Vec3b curColor = m_image.at< cv::Vec3b >(m_fgPixels[i].x, m_fgPixels[i].y);
		for(int j = 0; j < 3; ++j){
			melColor[j] += curColor.val[j];
		}
	} 
	for(int  i = 0; i < m_bgPixels.size(); ++i){
		cv::Vec3b curColor = m_image.at< cv::Vec3b >(m_bgPixels[i].x, m_bgPixels[i].y);
		for(int j = 0; j < 3; ++j){
			skinColor[j] += curColor.val[j];
		}
	} 
	for(int i = 0; i < 3; ++i){
		skinColor[i] /= m_bgPixels.size();
	}
	
	for(int i = 0; i < 3; ++i){
		melColor[i] /= m_fgPixels.size();
	}
	double skinSum = 0, melSum = 0;
	for(int  i = 0; i < 3; ++i){
		skinSum += skinColor[i];
		melSum += melColor[i];
	}
	double res = melColor[m_channel] / melSum - skinColor[m_channel] / skinSum;
	return res;
}


color_coordinates::color_coordinates( const cv::Mat& image, const cv::Mat& contoured_image): feature_detector(image, contoured_image) {}

color_coordinates::~color_coordinates() {}

double color_coordinates::get_feature() const 
{
}
std::vector< double >  color_coordinates::get_features()
{
	std::vector< double > m_features;
	double L, R, G, B, angleA, angleB, a, b, Lstar, X, Y, Z;
	R = G = B = 0;
	for(int  i = 0; i < m_fgPixels.size(); ++i){
		cv::Vec3b curColor = m_image.at< cv::Vec3b>( m_fgPixels[i].x, m_fgPixels[i].y);
		R += curColor.val[2];
		G += curColor.val[1];
		B += curColor.val[0];
	}
	R /= m_fgPixels.size();
	G /= m_fgPixels.size();
	B /= m_fgPixels.size();
	L = std::sqrt( R * R + G * G + B * B);
	angleA = 1.0 / std::cos( B / L);
	angleB = 1.0 / std::cos( R / ( L * std::sin(angleA)));
	//not sure about the ones calculated below, needs more investigation

	cv::Mat lab_image;
	cv::cvtColor( m_image, lab_image, CV_BGR2Lab);
	Lstar = a = b = 0;
	for(int i = 0; i < m_fgPixels.size(); ++i){
		cv::Vec3f curColor = lab_image.at< cv::Vec3f > ( m_fgPixels[i].x, m_fgPixels[i].y);
		Lstar += curColor.val[0];
		a += curColor.val[1];
		b += curColor.val[2];
	}

	Lstar /= m_fgPixels.size();
	a /= m_fgPixels.size();
	b /= m_fgPixels.size();
	m_features.push_back(L);
	m_features.push_back(angleA);
	m_features.push_back(angleB);
	m_features.push_back(Lstar);
	m_features.push_back(a);
	m_features.push_back(b);
	return m_features;
}
