#include "feature_extractor.h"

feature_extractor::feature_extractor( const cv::Mat& image)
{
	cv::cvtColor( image, m_image, CV_RGB2GRAY);
}

size_feature_extractor::size_feature_extractor( const cv::Mat& image): feature_extractor(image)
{

	get_contour_points();
//	m_mass_center = get_mass_center();

	new_window( image, "image");
	new_window( m_image, "m_image");
}


feature_extractor::~feature_extractor() {}


//size feature extractor
std::string size_feature_extractor::description() const 
{
	return " size features";
}


size_feature_extractor::~size_feature_extractor() {}

void size_feature_extractor::launch()
{
	m_features["asymmetry features"] = extract_asymmetry_index();


	//m_features["irregul;arity idnex"] = extract_irregularity_index();

}

std::map< std::string, double> size_feature_extractor::get_extracted_features( ) const 
{
	return m_features;
}

void size_feature_extractor::get_contour_points( )
{
	int px[8] = {0,0,1,-1,1,-1,1,-1};
	int py[8] = {-1,1,0,0,1,-1,-1,1};
	m_contour.clear();
	bool** mark = new bool*[m_image.rows];
	for(int i = 0; i < m_image.rows; ++i){
		mark[i] = new bool[m_image.cols];
		for(int j = 0; j < m_image.cols; ++j){
			mark[i][j] = false;
		}
	}

	mark[0][0] = true;
	std::queue< cv::Point> q;
	q.push( cv::Point (0,0));
	while( !q.empty()){
		int x = q.front().x;
		int y = q.front().y;
		q.pop();
		for(int i = 0; i < 8; ++i){
			int newX = x + px[i];
			int newY = y + py[i];
			if( is_valid_coordinate(m_image, newX, newY) && !mark[newX][newY]){
				mark[newX][newY] = true;
				if( m_image.at<uchar> (newX, newY) == 255){
					m_contour.push_back(cv::Point( newX, newY));
				} else{
					q.push( cv::Point( newX, newY));
				}
			}
		}
	}
	for(int i = 0; i < m_image.rows; ++i){
		delete[] mark[i];
	}
	delete[] mark;
}

double size_feature_extractor::extract_irregularity_index( ) const
{
	double area = get_contour_area();
	double perimeter = get_contour_perimeter();
//	perimeter = m_contour.size();
	std::cout<< "pi  "<<PI<<std::endl;
	double index = (perimeter * perimeter) / ( 4 * PI  * area);
	std::cout<<"index   "<<index<<std::endl;

	return index;
}

double size_feature_extractor::extract_asymmetry_index() const
{
	cv::Mat temp(m_image.size(), CV_8UC3);
	for(int i = 0; i < m_image.rows; ++i){
		for(int j = 0; j < m_image.cols; ++j){
			if( m_image.at<uchar>(i,j) != 0){
				temp.at< cv::Vec3b>(i,j) = cv::Vec3b( 255,255,255);
			}
		}
	}
	cv::Point2f center;
	float radius;
	cv::minEnclosingCircle(m_contour, center, radius);

	double circleArea = PI * radius * radius;
	double contourArea = cv::contourArea( m_contour );
	cv::Mat tempp = m_image.clone();

	cv::circle( tempp, center, radius, cv::Scalar( 124, 134, 134));
	new_window( tempp, "temp");

	std::cout<< "circle area   "<<circleArea<<"    contourArea  "<< contourArea<<std::endl;
	std::cout<<" radius is ";
	std::cout<< "the new asymmetry feature is now "<<1 - contourArea / circleArea;

}

int size_feature_extractor::get_contour_area() const
{
	int res = 0;
	for(int  i = 0; i < m_image.rows; ++i){
		for(int  j = 0; j < m_image.cols; ++j){
			if( m_image.at< uchar> (i,j) == 255){
				++res;
			}
		}
	}
	return res;
}

int size_feature_extractor::get_contour_perimeter() const
{
	int res = 0;
	int px[8] = {0,0,1,-1};
	int py[8] = {-1,1,0,0};
	bool** mark = new bool*[m_image.rows];
	for(int i = 0; i < m_image.rows; ++i){
		mark[i] = new bool[m_image.cols];
		for(int j = 0; j < m_image.cols; ++j){
			mark[i][j] = false;
		}
	}

	mark[0][0] = true;
	std::queue< cv::Point> q;
	q.push( cv::Point (0,0));
	while( !q.empty()){
		int x = q.front().x;
		int y = q.front().y;
		q.pop();
		for(int i = 0; i < 4; ++i){
			int newX = x + px[i];
			int newY = y + py[i];
			if( is_valid_coordinate(m_image, newX, newY) && !mark[newX][newY]){
				mark[newX][newY] = true;
				if( m_image.at<uchar> (newX, newY) != 0){
					++res;
				} else{
					q.push( cv::Point( newX, newY));
				}
			}
		}
	}
	for(int i = 0; i < m_image.rows; ++i){
		delete[] mark[i];
	}
	delete[] mark;
	return res;
}

cv::Point size_feature_extractor::get_mass_center( ) const
{
	cv::Moments moment;

	moment = cv::moments( m_image, true);
	double x = moment.m10 / moment.m00;
	double y = moment.m01 / moment.m00;
	cv::Mat temp = m_image.clone();
	cv::circle( temp, cv::Point( x,y) , 10, cv::Scalar( 0,0,0), -1);
	new_window( temp, "temp");
	return cv::Point( x,y);
}